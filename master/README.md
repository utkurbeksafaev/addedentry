# VueJS, MongoDB, Flask Login Registration
A login register sample application with MongoDB, VueJS, Flask

1. Run the server-side Flask app in one terminal window:

    ```sh
    $ cd server
    $ python3.6 -m venv env
    $ source env/bin/activate
    (env)$ pip3 install -r requirements.txt
    (env)$ pip3 install pandas
    (env)$ python3 mongo.py
    ```

    Navigate to [http://localhost:5000](http://localhost:5000)

1. Run the client-side Vue app in a different terminal window:

    ```sh
    $ cd client
    $ npm install
    $ npm run dev
    ```

    Navigate to [http://localhost:8080](http://localhost:8080)

