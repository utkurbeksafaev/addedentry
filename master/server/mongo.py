from flask import Flask, json, request, jsonify
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from pymongo import cursor
from datetime import datetime
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token
import pandas as pd
from werkzeug.utils import secure_filename
import json
import os
import uuid

UPLOAD_FOLDER = 'C:/Users/henri/Documents/RCLog/server/uploads' #A folder where all files are being stored in addition to db.
ALLOWED_EXTENSIONS = {'csv'} #Only allowing files in the type of cvs to be uploaded.

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'RCLog'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/RCLog'
app.config['JWT_SECRET_KEY'] = 'secret'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mongo = PyMongo(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

class mergeError:
    def __init__(self, usersObject, uId, loadedObject, lId):
        self.useObj = usersObject,
        self.uId = uId,
        self.loaObj = loadedObject,
        self.lId = lId

CORS(app)
@app.route('/hello', methods=['GET'])
def get():
        documents = []
        users = mongo.db.RCLogCol
        for c in users.find({}):
            c.pop("_id")
            documents.append(c)
        return jsonify(documents)
@app.route('/entries/<u_id>', methods=['GET', 'POST'])
def all_routes(u_id):
    response_object = {'status': 'success'}
    users = mongo.db.RCLogCol
    users.insert({
        'u_id': u_id,
        'id': uuid.uuid4().hex,
        'location': "Lillehammer",
        'date': '15/09/2019',
        'geo': "lat: 123 long: 345",
        'routename': 'Jack Kerouac',
        'grade': "6+",
        'length': '12'
    })
    users.insert({
        'u_id': u_id,
        'id': uuid.uuid4().hex,
        'location': "Gjovik",
        'date': '13/09/2019',
        'geo': "lat: 13 long: 545",
        'routename': 'Lofoten',
        'grade': "9+",
        'length': '14'
    })
    if request.method == 'POST':
        post_data = request.get_json()
        users.insert({
            'u_id': u_id,
            'id': uuid.uuid4().hex,
            'location': post_data.get('location'),
            'date': post_data.get('date'),
            'geo': post_data.get('geo'),
            'routename': post_data.get('routename'),
            'grade' : post_data.get('grade'),
            'length' : post_data.get('length')
        })
    else:
        documents = []
        for c in users.find({'u_id': u_id}):
            c.pop("_id")
            documents.append(c)

        response_object['routes'] = documents

    return jsonify(response_object)

@app.route('/entries/<u_id>/<list_id>', methods=['PUT', 'DELETE'])
def single_route(u_id,list_id):
    response_object = {'status': 'success'}
    users = mongo.db.RCLogCol
    if request.method == 'PUT':
        post_data = request.get_json()
        remove_route(list_id)
        users.insert({
            'u_id': u_id,
            'list_id': uuid.uuid4().hex,
            'location': post_data.get('location'),
            'date': post_data.get('date'),
            'geo': post_data.get('geo'),
            'routename': post_data.get('routename'),
            'grade' : post_data.get('grade'),
            'length' : post_data.get('length')
        })
    if request.method == 'DELETE':
        remove_route(list_id)
    return jsonify(response_object)
def remove_route(list_id):
    users = mongo.db.RCLogCol
    which_one = {"list_id": list_id}
    users.delete_one(which_one)
    return True


@app.route('/users/register', methods=['POST'])
def register():
    users = mongo.db.RCLogCol
    first_name = request.get_json()['first_name']
    last_name = request.get_json()['last_name']
    email = request.get_json()['email']
    password = bcrypt.generate_password_hash(request.get_json()['password']).decode('utf-8')
    created = datetime.utcnow()
    licenses = []
    #I Removed the enries-list
    mergeConflict = []

    user_id = users.insert({
        'first_name': first_name,
        'last_name': last_name,
        'email': email,
        'password': password,
        'created': created,
        'licenses': licenses,
        'mergeConflicts': mergeConflict,
        'objectType': 'user'
    })

    new_user = users.find_one({'_id': user_id})

    result = {'email': new_user['email'] + ' registered'}

    return jsonify({'result' : result})

@app.route('/users/login', methods=['POST'])
def login():
    users = mongo.db.RCLogCol
    email = request.get_json()['email']
    password = request.get_json()['password']
    result = ""

    response = users.find_one({'email': email})

    if response:
        if bcrypt.check_password_hash(response['password'], password):
            access_token = create_access_token(identity = {
                'first_name': response['first_name'],
                'last_name': response['last_name'],
                'email': response['email'],
                'id': str(response['_id'])
            })
            result = jsonify({"token": access_token})
        else:
            result = jsonify({"error": "Invalid username and password"})
    else:
        result = jsonify({"result": "No results found"})
    return result


@app.route('/viewusers', methods=['GET','POST'])
def view_users():
    response_object = {'status': 'success'}
    find_post = mongo.db.RCLogCol.find()
    name_list = []
    i = 0
    for name in find_post:
        single_name = {
            i: name['first_name']+' '+name['last_name'],
            }
        name_list.append(single_name)
        print(name_list)
        i = i+1

    response_object['full_name'] = name_list

    return jsonify(response_object)

@app.route('/licenses/<username>', methods=["GET"])
def get_lisence(username):
    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    response_object['licenses'] =  find_post['lisence']

    return jsonify(response_object)

@app.route('/pushEntries/<username>', methods=["POST"])
def upload_csv_file(username):
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return jsonify({'result': 'No file part'})
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            return jsonify({'result': 'No selected file'})
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            import_csvfile_db(filepath, username)
            return jsonify({'result' : 'Climbentries added'})
    return 'result'

def import_csvfile_db(filepath, username): #Does not handle merge conflicts yet.
    myDB = mongo.db.RCLogCol
    cdir = os.path.dirname(__file__)
    file_res = os.path.join(cdir, filepath)
    data = pd.read_csv(file_res)
    dataInJSON = json.loads(data.to_json(orient='records'))
    cnt = 0
    for i in dataInJSON:
        myDB.insert_one({'user_id' : username, 'LogEntry' : dataInJSON[cnt]})
        cnt = cnt+1
    return jsonify('Entries in cvs-file added')

def allowed_file(filename): #Used for controlling the filename when uploading a file.
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS















##compares a csv files content with a users existing climbEntries,
## then saves the csv content with a connected merge conflict to server
## currently don't work
#@app.route('/pushEntries/<username>', methods=["POST"])
#def pushCSVtoConflict(username):
#    # get both lists and start creating list 3 containing all objects recieved from front end and those similar in db.
#    post_data = request.get_json()
#    loadEntries = request.get_json()['data']
#
#    response_object = {'status':'success or something'}
#    single_license = mongo.db.RCLogCol
#
#    find_post = single_license.find_one({'_id': ObjectId(username)})
#    #response_object['entries'] =  find_post['entries']
#    post = find_post['entries']
#
#    userList = mongo.db.RcLogCol
#    usersEntries = list(post)
#
#    _lId = 0
#    _uId = 0
#    mergeErrorsByAmount = []
#    if len(usersEntries) > 0:
#        for lRow in loadEntries:
#            foundError = False
#            for uRow in usersEntries:
#                # 0 logType,# 1 location,  # 2 date,  # 3 geo,
#                # 4 routeName, # 5 grade,  # 6 lenght
#                if uRow[1] == lRow[1] and uRow[2] == lRow[2] and uRow[4] == lRow[4]:
#                    mE =mergeError(uRow, _uId, lRow, _lId)
#                    mergeErrorsByAmount.append(mE)
#                    foundError = True
#            if not foundError:
#                mE = mergeError("0",_uId,lRow,_lId)
#                mergeErrorsByAmount.append(mE)
#            _uId = _uId + 1
#        _lId = _lId + 1
#    else:
#        for lRow in loadEntries:
#            mE = mergeError("0",0,lRow,_lId)
#            mergeErrorsByAmount.append(mE)
#            _lId = _lId + 1
#
#    userList.update_one({'_id': username}, {'$push':{'mergeConflict': mergeErrorsByAmount}})
#
#    return jsonify({'result' : 'entries added to merge conflict list'})



if __name__ == '__main__':
    app.run(debug=True)
