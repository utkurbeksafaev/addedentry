import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Profile from '@/components/Profile'
import entries from '@/components/user/entries'
import license from '@/components/user/license'
import ViewUsers from '@/components/ViewUsers'
import handleMerge from '@/components/user/handleMerge'
import loadRoutes from '@/components/user/loadRoutes'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/viewusers',
      name: 'ViewUsers',
      component: ViewUsers
    },
    {
      path: '/user/license',
      name: 'License',
      component: license
    },
    {
      path: '/user/entries',
      name: 'Entries',
      component: entries
    },
    {
      path: '/user/handleMerge',
      name: 'handleMerge',
      component: handleMerge
    },
    {
      path: '/user/loadRoutes',
      name: 'loadRoutes',
      component: loadRoutes
    }
  ]
})
