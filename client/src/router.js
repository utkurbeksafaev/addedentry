import Vue from 'vue';
import Router from 'vue-router';
import Books from './components/Books.vue';
import Ping from './components/Ping.vue';
import Logentry from './components/LogEntry.vue';
import Licenses from './components/Licenses.vue';
import Licensesshow from './components/Licensesshow.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/books',
      name: 'Books',
      component: Books,
    },
    {
      path: '/ping',
      name: 'Ping',
      component: Ping,
    },
    {
      path: '/',
      name: 'Logentry',
      component: Logentry,
    },
    {
      path: '/licenses',
      name: 'Licenses',
      component: Licenses,
    },
    {
      path: '/licensesshow',
      name: 'Licensesshow',
      component: Licensesshow,
    },
  ],
});
