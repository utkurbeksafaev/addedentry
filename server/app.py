import uuid
import pymongo
from flask import Flask, jsonify, request
from flask_cors import CORS
from pymongo import MongoClient
from bson import ObjectId


BOOKS = [
    {
        'id': uuid.uuid4().hex,
        'title': 'On the Road',
        'author': 'Jack Kerouac',
        'read': True
    },
    {
        'id': uuid.uuid4().hex,
        'title': 'Harry Potter and the Philosopher\'s Stone',
        'author': 'J. K. Rowling',
        'read': False
    },
    {
        'id': uuid.uuid4().hex,
        'title': 'Green Eggs and Ham',
        'author': 'Dr. Seuss',
        'read': True
    }
]

client = MongoClient('localhost', 27017)

db = client.RCLog
# single_license = db.RCLog_license


LICENSES = [
    {
        'id': uuid.uuid4().hex,
        'holder': 'User1',
        'nameoflicense': 'License1',
        'activefrom': '2018.9.10',
        'activeto': '2019.9.10'
    },
    {
        'id': uuid.uuid4().hex,
        'holder': 'User2',
        'nameoflicense': 'License2',
        'activefrom': '2019.1.10',
        'activeto': '2020.1.10'
    }
]

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

#ongo = PyMongo(app)


def remove_book(book_id):
    for book in BOOKS:
        if book['id'] == book_id:
            BOOKS.remove(book)
            return True
    return False

def remove_license(license_id):
    for license in LICENSES:
        if license['id'] == license_id:
            LICENSES.remove(license)
            return True
    return False


# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

# @app.route('logentries', methods=['GET'])
# def log_entry():
#     response_object = {'status': 'success'}
#     try:
#         u = document.find_one({'name': 'Ola Norman'})
#         response_object['user'] = u
#         return(jsonify(response_object))
#
#@app.route('/logentries1', methods=['GET'])
#def all_logentry1():
#    try:
#        # test function calling MongoDB.
#        data = json.loads(request.data)
#        user = db.RCLog.RCLogCol.find()
#        
#        response_object['logentry'] = dumps(user) # collection.find({"name":"Ola Norman"})
#        response_object = {'status': 'success'}
#    response_object = {'status': 'fail'}
#    response_object['logentry'] = "fail"
#     response_object['logentry'] = collection.find({"name":"Ola Norman"})
#     return jsonify(response_object)


# @app.route('/localhost:5000/licenses/<username>', methods=["GET"])
# def get_username(username):
#     response_object = {'status':'success or something'}
#     single_license = db.RCLog_license
#
#     # find_post = single_license.find_one({'holder': "Chow"})
#     find_post = single_license.find_one({'_id': ObjectId(username)})
#     #test_post = posts.find_one({'name': "Ola Norman"})
#     response_object['licenses'] =  find_post['license']
#     response_object['name'] = find_post['name']
#
#     response_object['id'] = username
#
#     # print(username)
#     # response_object['message'] = find_post['nameoflicense']
#
#     return jsonify(response_object)


@app.route('/books', methods=['GET', 'POST'])
def all_books():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        BOOKS.append({
            'id': uuid.uuid4().hex,
            'title': post_data.get('title'),
            'author': post_data.get('author'),
            'read': post_data.get('read')
        })
        response_object['message'] = 'Book added!'
    else:
        response_object['books'] = BOOKS
    return jsonify(response_object)

@app.route('/books/<book_id>', methods=['PUT', 'DELETE'])
def single_book(book_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        remove_book(book_id)
        BOOKS.append({
            'id': uuid.uuid4().hex,
            'title': post_data.get('title'),
            'author': post_data.get('author'),
            'read': post_data.get('read')
        })
        response_object['message'] = 'Book updated!'
    if request.method == 'DELETE':
        remove_book(book_id)
        response_object['message'] = 'Book removed!'
    return jsonify(response_object)


@app.route('/licenses', methods=['GET', 'POST'])
def all_licenses():
    response_object = {'status': 'success'}

    # if request.method == 'GET':
    #     print("getting")
    #     post_data = request.get_json()
    #     print(post_data)
    #     LICENSES.append({
    #         'id': uuid.uuid4().hex,
    #         'holder': post_data.get('holder'),
    #         'nameoflicense': post_data.get('nameoflicense'),
    #         'activefrom': post_data.get('activefrom'),
    #         'activeto': post_data.get('activeto')
    #     })
    #     response_object['message'] = 'license added!'
    #     return jsonify({"content": "this is a content"})

    if request.method == 'POST':
        post_data = request.get_json()
        print(post_data)
        LICENSES.append({
            'id': uuid.uuid4().hex,
            'holder': post_data.get('holder'),
            'nameoflicense': post_data.get('nameoflicense'),
            'activefrom': post_data.get('activefrom'),
            'activeto': post_data.get('activeto')
        })
        response_object['message'] = 'license added!'
    else:
        response_object['licenses'] = LICENSES
    return jsonify(response_object)


@app.route('/licenses/<license_id>', methods=['PUT', 'DELETE'])
def single_license(license_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        remove_license(license_id)
        LICENSES.append({
            'id': uuid.uuid4().hex,
            'holder': post_data.get('holder'),
            'nameoflicense': post_data.get('nameoflicense'),
            'activefrom': post_data.get('activefrom'),
            'activeto': post_data.get('activeto')
        })
        response_object['message'] = 'License updated!'
    if request.method == 'DELETE':
        remove_license(license_id)
        response_object['message'] = 'License removed!'
    return jsonify(response_object)




if __name__ == '__main__':
    app.run()
