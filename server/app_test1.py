import uuid
import pymongo
from flask import Flask, jsonify, request
from flask_cors import CORS
from pymongo import MongoClient
import json
from bson import ObjectId


LICENSES = [
    {
    #     'id': uuid.uuid4().hex,
    #     'holder': 'User1',
    #     'nameoflicense': 'License1',
    #     'activefrom': '2018.9.10',
    #     'activeto': '2019.9.10'
    # },
    # {
    #     'id': uuid.uuid4().hex,
    #     'holder': 'User2',
    #     'nameoflicense': 'License2',
    #     'activefrom': '2019.1.10',
    #     'activeto': '2020.1.10'
    }
]

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})


client = MongoClient('localhost', 27017)

db = client.RCLog
license_info = db.RCLog_license


@app.route('/licensesshow/5d7f31e5b4efc41038c370b6', methods=['GET', 'POST'])
def get_license_db():
    response_object = {'status': 'success'}
    post_data = license_info.find_one({'_id': ObjectId("5d7f31e5b4efc41038c370b6")})
    # post_data = license_info.find_one({'name': "Kari Norman"})
    print(post_data)
    # response_object = { 'id': post_data['_id']}
    response_object['id'] = str(post_data['_id'])
    response_object['name'] = post_data['name']
    response_object['license'] = post_data['license']
    return jsonify(response_object)


if __name__ == '__main__':
    app.run()
