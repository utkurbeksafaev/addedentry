from flask import Flask, jsonify, request, jsonify
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from datetime import datetime
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'RCLog'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/RCLog'
app.config['JWT_SECRET_KEY'] = 'secret'

mongo = PyMongo(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

class mergeError:
    def __init__(self, usersObject, uId, loadedObject, lId):
        self.useObj = usersObject,
        self.uId = uId,
        self.loaObj = loadedObject,
        self.lId = lId

CORS(app)

@app.route('/users/register', methods=['POST'])
def register():
    users = mongo.db.RCLogCol
    first_name = request.get_json()['first_name']
    last_name = request.get_json()['last_name']
    email = request.get_json()['email']
    password = bcrypt.generate_password_hash(request.get_json()['password']).decode('utf-8')
    created = datetime.utcnow()
    entries = []
    licenses = []
    mergeConflict = []

    user_id = users.insert({
        'first_name': first_name,
        'last_name': last_name,
        'email': email,
        'password': password,
        'created': created,
        'entries': entries,
        'licenses': licenses
    })

    new_user = users.find_one({'_id': user_id})

    result = {'email': new_user['email'] + ' registered'}

    return jsonify({'result' : result})

@app.route('/users/login', methods=['POST'])
def login():
    users = mongo.db.RCLogCol
    email = request.get_json()['email']
    password = request.get_json()['password']
    result = ""

    response = users.find_one({'email': email})

    if response:
        if bcrypt.check_password_hash(response['password'], password):
            access_token = create_access_token(identity = {
                'first_name': response['first_name'],
                'last_name': response['last_name'],
                'email': response['email'],
                'id': str(response['_id']),
                'objectType': response['objectType']
            })
            result = jsonify({"token": access_token})
        else:
            result = jsonify({"error": "Invalid username and password"})
    else:
        result = jsonify({"result": "No results found"})
    return result

@app.route('/licenses/<username>', methods=["GET"])
def get_lisence(username):
    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    response_object['licenses'] =  find_post['lisence']

    return jsonify(response_object)

@app.route('/entries/<username>', methods=["GET"])
def get_entries(username):
    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    response_object['entries'] =  find_post['entries']

    return jsonify(response_object)

@app.route('/entries/<username>', methods=["PUT"])
def post_entries(username):
    post_data = request.get_json()
    userList = mongo.db.RcLogCol
    #user = userList.find({'_id': ObjectId(username)})
    result = userList.update_one(
        {'_id': ObjectId(username)}, 
        {   "$push": {
                'entries': {
                    'logType': "climbEntry",
                    'location': request.get_json()['location'],
                    'geo': request.get_json()['geo'],
                    'date': request.get_json()['date'],
                    'routeName': request.get_json()['routeName'],
                    'grade': request.get_json()['grade'],
                    'length': request.get_json()['length']
                } 
            }
        },
        upsert=True
    ) 
    return jsonify({'result' : 'entry added'})

@app.route('/pushEntries/<username>', methods=["POST"])
def pushCSVtoConflict(username):
    # get both lists and start creating list 3 containing all objects recieved from front end and those similar in db.     
    post_data = request.get_json()
    loadEntries = request.get_json()['data'] 

    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    #response_object['entries'] =  find_post['entries']
    post = find_post['entries']

    userList = mongo.db.RcLogCol
    usersEntries = list(post)

    _lId = 0
    _uId = 0
    mergeErrorsByAmount = []
    if len(usersEntries) > 0:
        for lRow in loadEntries:
            foundError = False
            for uRow in usersEntries:
                # 0 logType,# 1 location,  # 2 date,  # 3 geo, 
                # 4 routeName, # 5 grade,  # 6 lenght
                if uRow[1] == lRow[1] and uRow[2] == lRow[2] and uRow[4] == lRow[4]:
                    mE =mergeError(uRow, _uId, lRow, _lId)
                    mergeErrorsByAmount.append(mE)
                    foundError = True
            if not foundError:
                mE = mergeError("0",_uId,lRow,_lId)
                mergeErrorsByAmount.append(mE) 
            _uId = _uId + 1
        _lId = _lId + 1
    else:
        for lRow in loadEntries:
            mE = mergeError("0",0,lRow,_lId)
            mergeErrorsByAmount.append(mE)
            _lId = _lId + 1
    
    userList.update_one({'_id': username}, {'$push':{'mergeConflict': mergeErrorsByAmount}})

    return jsonify({'result' : 'entries added to merge conflict list'})

if __name__ == '__main__':
    app.run(debug=True)

