import csv

with open('databaseFiles.csv') as csv_file:
    usersEntries = csv.reader(csv_file, delimiter=',')
with open('uploadedFile.csv') as csv_file:
    uploadedFile = csv.reader(csv_file, delimiter=',')

uLine = 1
class mergeError:
    def __init__(self, numErrors, uploadedObject, uId, mergeConflict, mId):
        self.numErr = numErrors,
        self.uppObj = uploadedObject,
        self.uId = uId,
        self.meConf = mergeConflict,
        self.mId = mId

#For sortering basert på antall errors. Hvert merge conflict vil ha flagget hvor mange likheter er her. 
mergeErrorsByAmount = []

for uRow in usersEntries:
    for lRow in uploadedFile:
        a = 0
        for i in range(7):
            if uRow[i] == lRow[i]:
                a = a + 1
        if a > 1:
            mE =mergeError(a,lRow,uRow)
            mergeErrorsByAmount.append(mE)

# for prioritert sammenligning.
mergeErrorsByTypeConflict = []

_mId = 0
_uId = 0
for uRow in usersEntries:
    for lRow in uploadedFile:
        # 0 logType,# 1 location,  # 2 date,  # 3 geo, 
        # 4 routeName, # 5 grade,  # 6 lenght
        if uRow[1] == lRow[1] and uRow[2] == lRow[2] and uRow[4] == lRow[4]:
            mE =mergeError(0,lRow,_mId, uRow,_uId)
            mergeErrorsByAmount.append(mE)
        _mId = _mId + 1
    _uId = _uId + 1


