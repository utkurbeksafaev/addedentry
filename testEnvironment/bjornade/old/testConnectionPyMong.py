import pymongo
from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client.RCLog

posts = db.RCLogCol

test_post = posts.find_one({'name': 'Ola Norman'})

print(test_post)

#(env) D:\Skole\2019_Host\ACSProject\PyMongoTest>python testConnectionPyMong.py
#One post: 5d808b676fcebd59cfa02cb3
#{'_id': ObjectId('5d7f2caae1bf711038e88277'), 'name': 'Ola Norman', 'entries': [{'logType': 'climbEntry', 'location': 'Lillehammer', 'date': '01/01/2001', 'geo': {'lat': '61', 'long': '10'}, 'routeName': 'Ut på tur', 'grade': '6+', 'length': '12'}, {'logtype': 'climbEntry', 'location': 'Lillehammer', 'date': '02/02/2002', 'geo': {'lat': '60', 'long': '10'}, 'routeName': 'Heisa', 'grade': '6-', 'lenght': '14'}]}