
from mongoengine import *
import datetime

connect('mongoengine_test', host='localhost', port='27017')

import datetime

class Post(Document):
    title = StringField(required=True, max_length=200)
    content = StringField(required=True)
    author = StringField(required=True, max_length=50)
    published = DateTimeField(default=datetime.datetime.now)