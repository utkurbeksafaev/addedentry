from flask import Flask, request, jsonify, json
from flask_pymongo import PyMongo 
from bson.objectid import ObjectId
from pymongo import cursor
from datetime import datetime 
from flask_cors import CORS 
from flask_bcrypt import Bcrypt 
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'RCLog'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/RCLog'
app.config['JWT_SECRET_KEY'] = 'secret'

mongo = PyMongo(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

CORS(app)

@app.route('/users/register', methods=['POST'])
def register():
    users = mongo.db.RCLogCol
    first_name = request.get_json()['first_name']
    last_name = request.get_json()['last_name']
    email = request.get_json()['email']
    password = bcrypt.generate_password_hash(request.get_json()['password']).decode('utf-8')
    created = datetime.utcnow()
    entries = []
    licenses = []

    user_id = users.insert({
        'first_name': first_name,
        'last_name': last_name,
        'email': email,
        'password': password,
        'created': created, 
        'entries': entries,
        'licenses': licenses
    })

    new_user = users.find_one({'_id': user_id})

    result = {'email': new_user['email'] + ' registered'}
    
    return jsonify({'result' : result})

@app.route('/users/login', methods=['POST'])
def login():
    users = mongo.db.RCLogCol
    email = request.get_json()['email']
    password = request.get_json()['password']
    result = ""

    response = users.find_one({'email': email})

    if response:
        if bcrypt.check_password_hash(response['password'], password):
            access_token = create_access_token(identity = {
                'first_name': response['first_name'],
                'last_name': response['last_name'],
                'email': response['email'],
                'id': str(response['_id'])
            })
            result = jsonify({"token": access_token})
        else:
            result = jsonify({"error": "Invalid username and password"})
    else:
        result = jsonify({"result": "No results found"})
    return result 


@app.route('/viewusers', methods=['GET','POST'])
def view_users():
    response_object = {'status': 'success'}
    find_post = mongo.db.RCLogCol.find()
    name_list = []
    i = 0
    for name in find_post:
        single_name = {
            i: name['first_name']+' '+name['last_name'],
            }
        name_list.append(single_name)
        print(name_list)
        i = i+1

    response_object['full_name'] = name_list

    return jsonify(response_object)


@app.route('/licenses/<username>', methods=["GET"])
def get_lisence(username):
    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    response_object['licenses'] =  find_post['lisence']

    return jsonify(response_object)

@app.route('/entries/<username>', methods=["GET"])
def get_entries(username):
    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    response_object['entries'] = find_post['entries']

    return jsonify(response_object)

@app.route('/entries/<username>', methods=["POST"])
def post_entries(username):
    post_data = request.get_json()
    userList = mongo.db.RcLogCol
    user = userList.find({'_id': ObjectId(username)})
    userList.update({'_id': username}, {'$push': {'entries': {
            'logType': "climbEntry",
            'location': request.get_json()['location'],
            'geo': request.get_json()['geo'],
            'date': request.get_json()['date'],
            'routeName': request.get_json()['routeName'],
            'grade': request.get_json()['grade'],
            'length': request.get_json()['length']
        } }}
    )
    return jsonify({'result' : 'entry added'})
    
if __name__ == '__main__':
    app.run(debug=True)
