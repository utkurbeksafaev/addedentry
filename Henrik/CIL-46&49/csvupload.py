import os
import pandas as pd
from flask import Flask, flash, request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename
from flask_pymongo import PyMongo
import json

# UPLOAD_FOLDER is the path to the selected folder where uploaded files will be stored.
# Below is an path used for testing on my personal cumputer - Henrik
UPLOAD_FOLDER = 'C:/Users/henri/Documents/RCLog/server/uploads'
ALLOWED_EXTENSIONS = {'csv'}

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MONGO_DBNAME'] = 'RCLog'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/rclog'

mongo = PyMongo(app)


def import_csvfile_db(filepath):
    uploadcsv = mongo.db.csvuploads #csvuploads is a local collection used for testing. - Henrik
    cdir = os.path.dirname(__file__)
    file_res = os.path.join(cdir, filepath)
    data = pd.read_csv(file_res)
    data_json = json.loads(data.to_json(orient='records'))
    uploadcsv.insert_many(data_json)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/csvupload', methods=['GET', 'POST'])
def upload_csv_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            import_csvfile_db(filepath)
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''





@app.route('/csvupload/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                                        filename)

if __name__ == '__main__':
    app.run()
