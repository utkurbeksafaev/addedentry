ifrom flask import Flask, jsonify, request, flash, request, redirect, url_for, send_from_directory
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from datetime import datetime
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token
import pandas as pd
from werkzeug.utils import secure_filename
import json
import os


UPLOAD_FOLDER = 'C:/Users/henri/Documents/RCLog/server/uploads' #A folder where all cvs-files are being stored in addition to db.
ALLOWED_EXTENSIONS = {'csv'} #Only allowing files in the type of cvs to be uploaded.


app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'RCLog'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/RCLog'
app.config['JWT_SECRET_KEY'] = 'secret'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mongo = PyMongo(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

CORS(app)

def import_csvfile_db(username, filepath):
    uploadcsv = mongo.db.RCLogCol
    cdir = os.path.dirname(__file__)
    file_res = os.path.join(cdir, filepath)
    data = pd.read_csv(file_res)
    data_json = json.loads(data.to_json(orient='records'))
    #uploadcsv.insert_many(data_json) - The content of the cvs-file is stored in its own document.
    uploadcsv.update({'_id': username}, {'$push': {'entries': { #Not sure if this will work....
            data_json
        } }}
    )
    return jsonify({'result' : 'Entries in cvs-file added'})

def allowed_file(filename): #Used for controlling the filename when uploading a file.
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/csvupload/<username>', methods=['GET', 'POST'])
def upload_csv_file(username):
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            import_csvfile_db(username, filepath)
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''

@app.route('/csvupload/<username>/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                                        filename)

@app.route('/users/register', methods=['POST'])
def register():
    users = mongo.db.RCLogCol
    first_name = request.get_json()['first_name']
    last_name = request.get_json()['last_name']
    email = request.get_json()['email']
    password = bcrypt.generate_password_hash(request.get_json()['password']).decode('utf-8')
    created = datetime.utcnow()
    entries = []
    licenses = []

    user_id = users.insert({
        'first_name': first_name,
        'last_name': last_name,
        'email': email,
        'password': password,
        'created': created,
        'entries': entries,
        'licenses': licenses
    })

    new_user = users.find_one({'_id': user_id})

    result = {'email': new_user['email'] + ' registered'}

    return jsonify({'result' : result})

@app.route('/users/login', methods=['POST'])
def login():
    users = mongo.db.RCLogCol
    email = request.get_json()['email']
    password = request.get_json()['password']
    result = ""

    response = users.find_one({'email': email})

    if response:
        if bcrypt.check_password_hash(response['password'], password):
            access_token = create_access_token(identity = {
                'first_name': response['first_name'],
                'last_name': response['last_name'],
                'email': response['email'],
                'id': str(response['_id'])
            })
            result = jsonify({"token": access_token})
        else:
            result = jsonify({"error": "Invalid username and password"})
    else:
        result = jsonify({"result": "No results found"})
    return result

@app.route('/licenses/<username>', methods=["GET"])
def get_lisence(username):
    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    response_object['licenses'] =  find_post['lisence']

    return jsonify(response_object)

@app.route('/entries/<username>', methods=["GET"])
def get_entries(username):
    response_object = {'status':'success or something'}
    single_license = mongo.db.RCLogCol

    find_post = single_license.find_one({'_id': ObjectId(username)})
    response_object['entries'] =  find_post['entries']

    return jsonify(response_object)

@app.route('/entries/<username>', methods=["POST"])
def post_entries(username):
    post_data = request.get_json()
    userList = mongo.db.RcLogCol
    user = userList.find({'_id': ObjectId(username)})
    userList.update({'_id': username}, {'$push': {'entries': {
            'logType': "climbEntry",
            'location': request.get_json()['location'],
            'geo': request.get_json()['geo'],
            'date': request.get_json()['date'],
            'routeName': request.get_json()['routeName'],
            'grade': request.get_json()['grade'],
            'length': request.get_json()['length']
        } }}
    )
    return jsonify({'result' : 'entry added'})


if __name__ == '__main__':
    app.run(debug=True)
